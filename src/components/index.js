import ng from 'angular';

import Card from './card';

export default ng.module('app.components', [Card])
  .name;
