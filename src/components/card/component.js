import './card.less';
import template from './card.html';
import controller from './controller';

export default {
  template,
  controller
};
