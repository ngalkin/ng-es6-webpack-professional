import ng from 'angular';

import CardComponent from './component';
import CardService from './service';

export default ng.module('app.components.card', [])
  .service('CardService', CardService)
  .component('card', CardComponent)
  .name;
