export default class CardController {
  constructor(CardService) {
    'ngInject';
    this.CardService = CardService;
  }

  onClick() {
    const result = this.CardService.list();
    alert(result);
  }
}
